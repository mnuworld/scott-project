'use strict';

angular
  .module('chirplerApp', ['ngRoute', 'ui.router', 'ui.bootstrap', 'ngAnimate', 'ngAnimate-animate.css', 'xeditable']);
