// top slider
  $("#flexiselDemo3").flexisel({
  visibleItems: 12,
  animationSpeed: 200,
  autoPlay: false,
  clone: false,
  autoPlaySpeed: 3000,            
  pauseOnHover: true,
  enableResponsiveBreakpoints: true,
  responsiveBreakpoints: { 
	  portrait: { 
		  changePoint:480,
		  visibleItems: 1
	  }, 
	  landscape: { 
		  changePoint:640,
		  visibleItems: 2
	  },
	  tablet: { 
		  changePoint:768,
		  visibleItems: 3
	  }
  }
  });
// for modal sign up free
		$('#open-modal').click(function(){
			$('#myModal').modal({show:true})
		});
// for modal invite		
		$('#invite-modal').click(function(){
			$('#invite').modal({show:true})
		});
// for modal credit		
		$('#credit-modal').click(function(){
			$('#credit').modal({show:true})
		});
// for modal upgrade		
		$('#upgrade-modal, #upgrade-modal-two').click(function(){
			$('#upgrade').modal({show:true})
		});
// For Select Style
 		$('.selectpicker').selectpicker();
// Some Click Prevent 
		$('.user-btn').click(function(e){
			e.preventDefault();
		});
		$('.sidebar-sub-menu-add-button').click(function(e){
			e.preventDefault();
			$(this).toggleClass('active');
		});
		$('.sidebar-sub-menu-add-moor-button').click(function(){
			$('.tobeup > ul').toggle();
		});

// Tooltip
		$('img[data-toggle="tooltip"]').tooltip({
		    animated: 'fade',
		    placement: 'bottom',
		});
        
// Active Menu item by click

// select 2nd list item
var liToSelect = 2;
$(".nav li:eq("+(liToSelect-1)+")").addClass("active");

// dynamically activate list items when clicked
$(".nav li").on("click",function(){
  $(".nav li").removeClass("active");
  $(this).addClass("active");
});
// expand upgrade information
$(document).ready(function(){
	$("#expand, #upgrade-expand").click(function(){
	  $(".expand-content").slideDown(400);
	  $('#expand, .hide-txt').fadeOut();
	});
	$("#collaps").click(function(){
	  $(".expand-content").slideUp(400);
	  $("#expand, .hide-txt").fadeIn();
	});
  });
// for put me
$(window).load(function () {
	$(".put-me").fadeIn(1000);
});
// Icon Animation
// $('.page-link').hover(function(){
// 	$(this).children('i').addClass('animated shake');
// }, function(){
// 	$(this).children('i').removeClass('animated shake');
// });

// facebook like button
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// twitter like button
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

// google plus like button
(function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = 'https://apis.google.com/js/platform.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();