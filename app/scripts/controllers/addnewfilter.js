var myapp = angular.module("chirplerApp", ['ngRoute', 'ui.router', 'ui.bootstrap', 'ngAnimate', 'ngAnimate-animate.css', 'xeditable']);

myapp.run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});

myapp.controller('ReligiousCtrl', function($scope) {
  $scope.user = {
    name: 'Chiristian'
  };  
});

myapp.controller('newfilterController', function ($scope, $compile) {

    $scope.cloneField = function (event) {
        var fieldHtml = $(event.target).parents().find(".tobeclone").first().clone();
        var compiledElement = $compile(fieldHtml)($scope);
        $(event.target).parents().find(".tobeup").before(compiledElement);
    };
});
myapp.config(['$routeProvider', function ($routeProvider) {

  /**
   * $routeProvider
   */
  $routeProvider
        // route for the home page
      .when('/', {
        templateUrl : 'views/meet.html',
      })
      // route for the about page
      .when('/profile', {
        templateUrl : 'views/profile.html',
      })
      .when('/meet', {
        templateUrl : 'views/meet.html',
      })
      // route for the about page
      .when('/find', {
        templateUrl : 'views/find.html',
      })
      // route for the about page
      .when('/message', {
        templateUrl : 'views/message.html',
    })
      // route for the about page
      .when('/annonymous', {
        templateUrl : 'views/annonymous.html',
      })
      // route for the about page
      .when('/profile-view', {
        templateUrl : 'views/profile-view.html',
      })
      // route for the about page
      .when('/rating', {
        templateUrl : 'views/rating.html',
      })
      .otherwise({
       redirectTo: '/',

      });

      // configure html5 to get links working on jsfiddle
      // $locationProvider.html5Mode(true);
  }]);